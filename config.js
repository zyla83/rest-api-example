module.exports = {
    port: 3005,
    db: 'mongodb://localhost/dnaRestApiDb',
    dateFormat: 'YYYY-MM-DD',
    msgs: {
        serverStartInfo: 'RESTful API server started on:',
        notFound: ' not found',
        userNotExisitsError: 'User {id} not exists',
        userAlreadyExisitsError: 'User {id} already exists',
        userDeleteError: 'User {id} not deleted',
        userDeleteOk: 'User {id} deleted',
        fieldValidationError: 'Please fill in all necessary fields',
        dateFormatError: 'end date and start date must be in format {format}',
        dateComparsionError: 'end date must be later than start date'
    }
};
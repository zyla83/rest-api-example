const config = require('./config');
const port = process.env.PORT || config.port;
const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');
const api = express();

//users entitites
const Users = require('./api/models/usersModel');
const usersRoutes = require('./api/routes/usersRoutes');

//jobs entities
const Jobs = require('./api/models/jobsModel');
const jobsRoutes = require('./api/routes/jobsRoutes');

//lets enable mongoose promises
mongoose.Promise = global.Promise;
mongoose.connect(config.db, { useNewUrlParser: true });

//https://mongoosejs.com/docs/deprecations.html
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

//allow CORS
api.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Vary', 'Origin');
    res.header('Access-Control-Allow-Headers', '*');
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
        return res.status(200).json({});
    }
    next();
});

//lets add support to parsing of application/x-www-form-urlencoded post data
api.use(bodyParser.urlencoded({
    extended: true
}));

//lets add support to parsing of application/json type post data
api.use(bodyParser.json());


//register routes in express
usersRoutes(api);
jobsRoutes(api);

//default 404 msg
api.use((req, res) => {
    res.status(404).send({
        url: req.originalUrl + config.msgs.notFound
    });
});

api.listen(port);
console.log(config.msgs.serverStartInfo, config.port);
'use strict';
const jobs = require('../controllers/jobsController');
module.exports = function (api) {

    api.route('/jobs')
        .get(jobs.getAll)
        .post(jobs.create);

    api.route('/jobs/category/:category')
        .get(jobs.getByCategory);

    api.route('/jobs/employer/:employer')
        .get(jobs.getByEmployer);
};
'use strict';

module.exports = function (api) {
    const users = require('../controllers/usersController');
    api.route('/users')
        .get(users.getAll)
        .post(users.create);

    api.route('/users/:login')
        .get(users.get)
        .put(users.update)
        .delete(users.delete);
};
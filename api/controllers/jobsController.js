'use strict';

const config = require('../../config');
const moment = require('moment');
const mongoose = require('mongoose');
const Jobs = mongoose.model('Jobs');
const Users = mongoose.model('Users');

function _findIdByUserLogin(data) {
    return new Promise((resolve, reject) => {
        Users.findOne({
            login: data.login
        }).then(queryRes => {
            return resolve(queryRes._id);
        }).catch(err => {
            return reject(err);
        });
    });
}

function _createJobQuery(data) {
    return new Promise((resolve, reject) => {
        data.req.body.employer = data.id; //replacing name with key id

        try {
            if (moment(data.req.body.startDate, config.dateFormat, true).isValid() === false ||
                moment(data.req.body.endDate, config.dateFormat, true).isValid() === false) {
                const err = {
                    name: 'dateFormatError'
                };
                return reject(err);
            }

        } catch {
            return reject({
                name: 'dateFormatError'
            });
        }

        if (moment(data.req.body.endDate).isBefore(data.req.body.startDate)) {
            const err = {
                name: 'dateComparsionError'
            };
            return reject(err);
        }
        const query = new Jobs(data.req.body);
        query.save()
            .then(queryRes => {
                return resolve(queryRes);
            })
            .catch(err => {
                return reject(err);
            });
    });
}

const jobGetAll = function (req, res) {
    Jobs.find()
        .then(job => {
            res.json(job);
        })
        .catch(err => {
            res.json(err);
        });
};
const jobCreate = function (req, res) {

    _findIdByUserLogin({
        login: req.body.employer
    })
        .then(id => {
            return _createJobQuery({
                id: id,
                req: req
            });
        })
        .then(queryRes => {
            res.json(queryRes);
        })
        .catch(err => {
            let msgContent;
            switch (true) {
                case err.name === 'dateComparsionError':
                    msgContent = config.msgs.dateComparsionError;
                    break;
                case err.name === 'dateFormatError':
                    msgContent = config.msgs.dateFormatError.replace('{format}', config.dateFormat);
                    break;
                case err.name === 'ValidationError':
                    msgContent = config.msgs.fieldValidationError;
                    break;

                case req.body.employer === undefined:
                    msgContent = config.msgs.fieldValidationError;
                    break;

                default:
                    msgContent = config.msgs.userNotExisitsError.replace('{id}', req.body.employer);
            }
            res.status(500).json({
                message: msgContent
            });

        });
};

const jobGetByField = function (req, res) {
    let field = {};
    for (const param in req.params) {
        field[param] = req.params[param];
    }

    Jobs.find(field)
        .then(queryRes => {
            res.json(queryRes);
        })
        .catch(err => {
            res.send(err);
        });
};

const jobGetByCategory = function (req, res) {

    Jobs.find({ category: req.params.category }).select('-category')
        .then(queryRes => {
            const formattedResponse = {
                category: req.params.category,
                jobs: queryRes
            };
            res.json(formattedResponse);
        })
        .catch(err => {
            res.send(err);
        });
};

const jobGetByEmployer = function (req, res) {
    _findIdByUserLogin({
        login: req.params.employer
    })
        .then(id => {
            return new Promise((resolve, reject) => {
                Jobs.find({ employer: id }).select('-employer')
                    .then(queryRes => {
                        const formattedResponse = {
                            employer: req.params.employer,
                            employerId: id,
                            jobs: queryRes
                        };
                        return resolve(formattedResponse);
                    })
                    .catch(err => {
                        return reject(err);
                    });
            });
        })
        .then((jobsRes) => {
            res.json(jobsRes);
        })
        .catch(err => {
            res.status(500).json({
                message: config.msgs.userNotExisitsError.replace('{id}', req.params.employer)
            });
        });
}

module.exports = {
    getAll: jobGetAll,
    create: jobCreate,
    getByCategory: jobGetByCategory,
    getByEmployer: jobGetByEmployer
};
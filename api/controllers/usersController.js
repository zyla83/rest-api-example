'use strict';
const config = require('../../config');
const mongoose = require('mongoose');
const Users = mongoose.model('Users');

const userGetAll = function (req, res) {
    Users.find().select('-password -__v -jobs')
        .then(queryRes => {
            res.json(queryRes);
        })
        .catch(err => {
            res.json(err);
        });
};

const userCreate = function (req, res) {
    const query = new Users(req.body);
    query.save()
        .then(queryRes => {
            res.json(queryRes);
        })
        .catch(err => {
            let msgContent;
            switch (err.name) {
                case 'ValidationError':
                    msgContent = config.msgs.fieldValidationError;
                    break;
                default: //MongoError, lets assume that user already exists
                    msgContent = config.msgs.userAlreadyExisitsError.replace('{id}', req.body.login);
            }
            res.status(500).json({
                message: msgContent
            });
        });
};

const userGet = function (req, res) {
    Users.find({ login: req.params.login }).select('-password -__v')
        .then(queryRes => {
            res.json(queryRes);
        })
        .catch(err => {
            res.status(500).json({
                message: err
            });
        });
};

const userUpdate = function (req, res) {
    //https://mongoosejs.com/docs/api.html#model_Model.findOneAndUpdate
    Users.findOneAndUpdate({
        login: req.params.login
    }, req.body, {
            new: true
        })
        .then(queryRes => {
            res.json(queryRes);
        })
        .catch(err => {
            res.status(500).json({
                message: err
            });
        });
};


const userDelete = function (req, res) {
    Users.deleteOne({
        login: req.params.login
    })
        .then(queryRes => {
            let msgContent;
            if (queryRes.deletedCount === 1) {
                msgContent = config.msgs.userDeleteOk.replace('{id}', req.params.login);
            } else if (queryRes.deletedCount === 0) {
                res.status(409); // 409? https://tools.ietf.org/html/rfc7231#section-6.5.8
                msgContent = config.msgs.userDeleteError.replace('{id}', req.params.login);
            }
            res.json({
                message: msgContent
            });
        })
        .catch(err => {
            res.status(500).json({
                message: err
            });
        });

};
module.exports = {
    getAll: userGetAll,
    create: userCreate,
    get: userGet,
    update: userUpdate,
    delete: userDelete
};
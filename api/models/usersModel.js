'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;


const UsersSchema = new Schema({
    login: {
        type: String,
        required: 'Login is required',
        unique: true
    },
    password: {
        type: String,
        required: 'Password is required',
    },
    creationDate: {
        type: Date,
        default: Date.now
    }
});

// https://www.mongodb.com/blog/post/password-authentication-with-mongoose-part-1
UsersSchema.pre('save', async function save(next) {
    if (!this.isModified('password')) {
        return next();
    }
    try {
        const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
        this.password = await bcrypt.hash(this.password, salt);
        return next();
    } catch (err) {
        return next(err);
    }
});

module.exports = mongoose.model('Users', UsersSchema);
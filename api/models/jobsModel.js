'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const JobsSchema = new Schema({
    employer: {
        type: String,
        required: 'Employer is required'
    },
    title: {
        type: String,
        required: 'Job title is required'
    },
    category: {
        type: [{
            type: String,
            enum: ['uncategorized', 'IT', 'Food&Drinks', 'Office', 'Courier', 'Shop assistant']
        }],
        default: ['uncategorized']
    },
    startDate: {
        type: String,
        required: 'Start date is required'
    },
    endDate: {
        type: String,
        required: 'End date is required'
    }
});
module.exports = mongoose.model('Jobs', JobsSchema);
# DNArestApi

# 1. Configuration is made in config.js file:
    * port and url to mongoDB base, messages content
    * database copy is saved in folder /mongoDB, it needs to be instaled malually
    * to install api: npm i
    * to run api: npm run start

# 2. Endpoints:
## /users
#### methods:
    POST - creates new user(fields:
        * login - string, unique, required;
        * password - string, required,
        * creationDate - date )
    GET - gets all users

## /users/:login
#### methods:
    GET - gets all data about user [:login]
    PUT (or mabye should be PATCH instead?) - updates data for user [:login]
    DELETE - deletes user [:login]

## /jobs
#### methods:
    POST - creates new job offer (fields:
        * employer - required, string, employer name must be added as user to db before adding job offer. This record saves user _id key)
        * title - required,
        * category -  enum ['uncategorized', 'IT', 'Food&Drinks', 'Office', 'Courier', 'Shop assistant']
        * startDate - date in format YYYY-MM-DD
        * endDate - date in format YYYY-MM-DD
    GET - gets all jobs

## /jobs/category/:category
#### methods:
    GET - gets all jobs with category [:category]

## /jobs/employer/:employer
#### methods:
    GET - gets all jobs posted by employer [:employer]

# 3. Todo:
 - Learn how to secure mongo database
 - Rethink and learn how to make appropriate mongoose style relation between users and jobs collections (Schema.Types.ObjectId)
 - Learn how to make pagination in mongoose query
 - Learn how to password after update user data
 - Delete jobs connected with deleted user
 - Make CORS whitelist
 - Find how to make case insensitive query (category/IT and category/it should return exact results)
 - Learn how to make automated test
 - Rethink all one more time :)
